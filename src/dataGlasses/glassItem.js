import React, { Component } from 'react'

export default class glassItem extends Component {
  render() {
    return (
      <div>
        <p className="title"> ${this.props.glasses.id} - ${this.props.glasses.name} - ${this.props.glasses.brand} ( ${this.props.glasses.color})</p>
            <div className="row">
            <p className="price col-4">$${this.props.glasses.price}</p>
           <p className="status col-4">Stocking</p>
          </div>
        <p className="desc">${this.props.glasses.description}</p>
      </div>
    )
  }
}
