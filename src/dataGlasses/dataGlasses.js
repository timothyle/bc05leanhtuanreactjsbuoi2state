import React, { Component } from 'react'
import {dataGlasses} from "./danhsachkinh.js"
import CardItem from "./glassCard.js";


export default class RenderGlassesMap extends Component {
    state = {
        img1:"./img/model.jpg",
        img2:"./img/v1.png",
        thongtinHienThi:null,
    }
        renderGlassesList = () => {
        return dataGlasses.map((item, index, virtualImg) => {
            return (
                    <CardItem glasses={item} key={index} virtualGlasses={virtualImg}/>

        );    
        })
        };
        thaydoiKinh = (brand,name,price) => {
            console.log(brand,name,price);
            this.setState({
                thongtinHienThi:<div style={{padding:"50px",position:"relative",width:"42%",height:"200px",left:"40%",top:"168px",backgroundColor:"black",opacity:"0.5",color:"white"}}>
                <p className="title">{name} - {brand}</p>
                    <div className="row">
                        <p className="price col-4">${price}</p>
                        <p className="status col-4">Stocking</p>
                    </div>
            </div>
            });
        };
        doiKinh = (virtualImg) => {
            console.log(virtualImg);
            this.setState({
                img2:`${virtualImg}`
            });
        };
        render() {
        return (
            <div className='container'>
            <div className="row">
                <img className="vglasses__items" src="./img/g1.jpg" id="G1" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v1.png");this.thaydoiKinh("Armani Exchange","Bamboo wood",150)}}/>   
                <img className="vglasses__items" src="./img/g2.jpg" id="G2" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v2.png");this.thaydoiKinh("Arnette","American flag",150)}}/>   
                <img className="vglasses__items" src="./img/g3.jpg" id="G3" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v3.png");this.thaydoiKinh("Burberry","Belt of Hippolyte",100)}}/>   
                <img className="vglasses__items" src="./img/g4.jpg" id="G4" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v4.png");this.thaydoiKinh("Coarch","Cretan Bull",100)}}/>   
                <img className="vglasses__items" src="./img/g5.jpg" id="G5" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v5.png");this.thaydoiKinh("D&G","JOYRIDE",180)}}/>   
                <img className="vglasses__items" src="./img/g6.jpg" id="G6" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v6.png");this.thaydoiKinh("Polo","NATTY ICE",120)}}/>   
                <img className="vglasses__items" src="./img/g7.jpg" id="G7" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v7.png");this.thaydoiKinh("Ralph","TORTOISE",120)}}/>   
                <img className="vglasses__items" src="./img/g8.jpg" id="G8" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v8.png");this.thaydoiKinh("Polo","NATTY ICE",120)}}/>   
                <img className="vglasses__items" src="./img/g9.jpg" id="G9" alt="hình ảnh kính" style={{height:"200px",width:"200px"}} onClick={()=>{this.doiKinh("./img/v9.png");this.thaydoiKinh("Coarch","MIDNIGHT VIXEN REMIX",120)}}/>   
            </div>
            <div>
                {()=>{this.thaydoiKinh(this.state.img2)}}
            </div>
            <div className="row">
            <div style={{height:"40%",width:"40%",float:"left"}}>
            <img src={this.state.img1} alt=""  style={{padding:"50px"}} />
            </div>
            <div style={{height:"40%",width:"40%",float:"left"}}>
            <img src={this.state.img1}  alt=""  style={{padding:"50px",position:"absolute"}} />
            <img src={this.state.img2} alt="" id={this.state.img2.id}  style={{paddingTop:"171px",paddingLeft:"70px",height:"260px",width:"330px",top:"25px",left:"92px",opacity:"0.8",position:"relative"}} />
            <div>{this.state.thongtinHienThi}</div>
            </div>
            </div>
            </div>

        )
        }
}
