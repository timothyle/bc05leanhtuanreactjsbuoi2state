import './App.css';
import RenderGlassesMap from './dataGlasses/dataGlasses';
import background from './background.jpg'

function App() {
  return (
    <div className="App" style={{backgroundImage:`url(${background})`,backgroundSize:'cover',backgroundRepeat:"no-repeat",backgroundPosition:'center',width:'100vw',height:'100vh'}}>
      <RenderGlassesMap/>
    </div>
  );
}

export default App;
